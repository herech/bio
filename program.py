#!/usr/bin/python3
try:
    from scipy import ndimage
    from scipy import misc
    from scipy.ndimage import gaussian_filter
    from numpy import ma
    import numpy as np
    import os, sys, getopt

    from skimage import data, img_as_float
    from skimage import img_as_ubyte
    from skimage.filters import threshold_adaptive
    from skimage import feature
    from skimage.filters import threshold_otsu
    from skimage.color import convert_colorspace
    from skimage import filters
    from skimage.util.dtype import dtype_range
    from skimage.util import img_as_ubyte
    from skimage import exposure
    from skimage.morphology import disk
    from skimage.filters import rank
    from skimage import color
    from skimage.morphology import convex_hull_image
    from skimage import morphology
    from skimage import dtype_limits
    from skimage import data
    from skimage import measure
    from skimage import img_as_float
    from skimage.morphology import reconstruction

except Exception as e:
    print(e)
    
else:

    ''' NEPOUZIVAM, NEFUNGUJE TO MOC DOBRE, ALE MYSLENKA JE TAKOVA, ZE KDYBY SE NENASLY ZLUTE LEZE, ZKUSI SE NAJIT POMOCI TETO FUNKCE CERVENE SKRVNY
    def find_red_spots(img):
        img2 = ndimage.median_filter(img, 20) 
        img = img - img2

        #img,cdf = histeq(img)

        for rownum in range(len(img)):
           for colnum in range(len(img[rownum])):
              img[rownum][colnum] = abs(img[rownum][colnum])

        #dx = ndimage.sobel(img, 0,mode='wrap')  # horizontal derivative
        #dy = ndimage.sobel(img, 1,mode='wrap')  # vertical derivative
        #mag = np.hypot(dx, dy)  # magnitude
        #mag *= 255.0 / np.max(mag)  # normalize (Q&D)
        #img = mag

        #plt.hist(img.ravel(), 256)
        #plt.show()

        tmp_arrray = np.zeros((img.shape[0], img.shape[1])) # init 2D numpy array
        for rownum in range(len(img)):
            for colnum in range(len(img[rownum])):
                if (img[rownum][colnum] > 10):
                    tmp_arrray[rownum][colnum] = 1
                else:
                    tmp_arrray[rownum][colnum] = 0

        tmp_arrray = ndimage.binary_erosion(tmp_arrray, iterations=1)
        tmp_arrray = ndimage.binary_opening(tmp_arrray)
        tmp_arrray = ndimage.binary_dilation(tmp_arrray, iterations=5)
        tmp_arrray = ndimage.binary_opening(tmp_arrray)         
        img = tmp_arrray

        return img
    '''

    # zdroj:http://stackoverflow.com/questions/287871/print-in-terminal-with-colors-using-python
    def prRed(prt): print("\033[91m{}\033[00m" .format(prt))
    def prGreen(prt): print("\033[92m{}\033[00m" .format(prt))
    def prYellow(prt): print("\033[93m{}\033[00m" .format(prt))
    def prLightPurple(prt): print("\033[94m{}\033[00m" .format(prt))
    def prPurple(prt): print("\033[95m{}\033[00m" .format(prt))
    def prCyan(prt): print("\033[96m{}\033[00m" .format(prt))
    def prLightGray(prt): print("\033[97m{}\033[00m" .format(prt))
    def prBlack(prt): print("\033[98m{}\033[00m" .format(prt))

    def mark_objects_margins_in_image(img, original_img):
        '''
        Funkce zvyrazni objekty v obrazku original_img podle jeho prahovanrho vzoru a to tak, ze kolem nich vykresli barevny okraj
        @img vstupni obrazek, ktery slouzi jako vzor/maska podle ktere se budou urcovat objekty a jeich okraje
        @original_img vstupni obrazek, ke budem vyznacovat okraje objektu
        @return obrazek ktery zvyraznuje objkety v obrazku podle zadaneho vzoru/masky
        '''

        # ZELENE ZVYRAZNENI okraju objektu pomoci operatoru detekce hran
        img = filters.sobel(img)

        for rownum in range(len(original_img)):
            for colnum in range(len(original_img[rownum])):
                if (img[rownum][colnum] != 0):
                    original_img[rownum][colnum][0] = 0
                    original_img[rownum][colnum][1] = 255
                    original_img[rownum][colnum][2] = 0

        return original_img

    def get_mask(img):
        '''
        Funkce vrati vypocitanou masku (True pro popredi (sitnice), False pro pozadi (cerne))
        @img vstupni obrazek na kterem budu pocitat masku, predpokladame obrazek ktery je puvodni, ale zmenseny a korektovan (gamma, log, adjust_intenzity)
        @return vypocitana maska (True pro popredi (sitnice), False pro pozadi (cerne))
        '''

        # inicializujeme vystupni masku
        tmp_mask = np.zeros((img.shape[0], img.shape[1]), dtype=np.uint8) # init 2D numpy array

        # prevedeme na stupne sedi
        img = color.rgb2gray(img)
        # rozmazeme detaily (hlavne kvuli pozadi cernemu ktere je nehomogenni ve skutecnosti)
        img = ndimage.median_filter(img, 20)
        # najdeme obrys sitnice pomoci detekce hran canny
        img = feature.canny(img, sigma=0.1, low_threshold = 0.03, high_threshold = 0.08)
        # obrys sitnice spojime, aby v nem nebyly trhliny pripadne
        img = morphology.closing(img, morphology.square(10))
        
        
        # od leve hranice sitnice (hodota True (1)) (detekovane hrany) priradime bilou barvu 255 az po pravy okraj obrazku
        # pred levou hranici úrirazujeme pixelum cernou barvu 0
        for rownum in range(len(img)):
            left_boundary_found = False
            for colnum in range(len(img[rownum])):
                if (left_boundary_found == False):
                    if (img[rownum][colnum] == 1):
                        left_boundary_found = True;
                        tmp_mask[rownum][colnum] = 255
                    else:
                        tmp_mask[rownum][colnum] = 0
                # od hranice priradime same jednicky doprava        
                else:
                    tmp_mask[rownum][colnum] = 255

        # od praveho kraje k prave hranici sitnice priradime same 0, cimz ziskame masku
        for rownum in range(len(img)):
            columns_count = len(img[rownum])    
            for colnum in range(columns_count):
                if (img[rownum][columns_count - 1 - colnum] == 1):
                    break
                else:
                    tmp_mask[rownum][columns_count - 1 - colnum] = 0

        # ziskanou masku lehce zmensime, abychom si byli jisti zejsme uvnitr sitnice
        tmp_mask = morphology.erosion(tmp_mask, morphology.square(20))
        
        # Z masky jejiz pixely jsou bud 0 nebo 255 udělame masku kde pixeli jsou bud False (odpovida 0) nebo True (odpovida 255)   
        for rownum in range(len(tmp_mask)):
            for colnum in range(len(tmp_mask[rownum])):
                if (tmp_mask[rownum][colnum] == 255):
                    tmp_mask[rownum][colnum] = True
                else:
                    tmp_mask[rownum][colnum] = False

        return tmp_mask
        
    def find_yellow_lesions(img, img_mask):
        '''
        Funkce nalezne (ve forme biliych fleku na cernem podkladu) zlute leze v obrazku sitnice
        @img vstupni obrazek na kterem budu hledat zlute leze, predpoklada se ze vstupni obrazek je extrahovany zeleny kanal RGB puvodniho obrazku
        @img_mask maska obrazku, tedy False pro cerne pozadi a True pro popredi (samotnou sitnici)
        @return obrazek ktery obsahuje bile fleky na cernem pozadi
        '''

        img = ndimage.median_filter(img, 4) # mediánový filtr rozmaže nepotřebné detaily

        # nastavim na bilou 255 pozadi masky, ktere bylo puvodne cerne, aby mi to nedelalo u adaptivnio treesholdingu problemy
        for rownum in range(len(img_mask)):
            for colnum in range(len(img_mask[rownum])):
                if (img_mask[rownum][colnum] == False):
                   img[rownum][colnum] = 255

        # Adaptivní prahovani
        img = threshold_adaptive(img, 31, offset=-25, method='median', mode='nearest')


        # vratim zpatky cerne pozadi (puvodne nebylo ciste cerne, nyni uz je) do obrazku sitnice
        for rownum in range(len(img_mask)):
            for colnum in range(len(img_mask[rownum])):
                if (img_mask[rownum][colnum] == False):
                   img[rownum][colnum] = 0

        # nasledne odstranim drobne bile castecky a ty ktere zustaly naopak zvetsim
        img = ndimage.binary_opening(img)
        #img = ndimage.binary_dilation(img, iterations=3)

        return img

    def mask_optic_disk(img_with_yellow_lesion_extracted, img_green_channel):
        '''
        Funkce vrati obrazek s bilymi fleky ktere predstavuji svetle leze, bile fleky ktere predatvovali opticky disk jsou vymaskovany
        @img vstupni obrazek binarizovany, kde jsou bile fleky predatvujici leze a opticky disk a cerny pokdlak
        @return obrazek s bilymi fleky ktere predstavuji svetle leze, tedy obrazek s vymaskovanymi bilymi fleky ktere predstavovaly opticky disk 
        '''
        # maskovany obrazek bude vychazet z obrazku na vstupu, kde budou urcite aprtie zacerneny (0)
        masked_img = img_with_yellow_lesion_extracted

        # rozmazeme zeleny kanal puvodniho obrazku abychom snizili jas svetlym lezim a lepe se nam detekoval opticky disk
        img = ndimage.median_filter(img_green_channel, 30)

        # zvyraznime svetle oblasti (opticky disk)
        img = ndimage.maximum_filter(img,footprint=disk(20), mode='constant')
        img = ndimage.maximum_filter(img,footprint=disk(20), mode='constant')

        # BEGIN zdroj: http://scikit-image.org/docs/dev/auto_examples/color_exposure/plot_regional_maxima.html#sphx-glr-auto-examples-color-exposure-plot-regional-maxima-py
        # zde zkratka jeste vice zvyraznime regionalni maxima (opticky disk)
        image = img_as_float(img)
        image = gaussian_filter(image, 1)

        seed = np.copy(image)
        seed[1:-1, 1:-1] = image.min()
        mask = image

        dilated = reconstruction(seed, mask, method='dilation')

        img_substract = image - dilated
        # END zdroj: http://scikit-image.org/docs/dev/auto_examples/color_exposure/plot_regional_maxima.html#sphx-glr-auto-examples-color-exposure-plot-regional-maxima-py

        # prevedeme obrazek ve float na obrazek v ubyte, vypneme warning, rpottoze nas to varuje ze ztracime presnost
    
        img = img_as_ubyte(img_substract)

        
        img_max = img.max() # maximalni hodnota v obrazku (opticky disk je slozen jen z techto hodnot)

        # prahujeme, tedy maximalni hodnoty (z nich je slozen opticky disk) budou 1 a ostatni budou 0
        for rownum in range(len(img)):
            for colnum in range(len(img[rownum])):
                if (img[rownum][colnum] == img_max):
                    img[rownum][colnum] = 1
                else:
                    img[rownum][colnum] = 0
        
        #img = ndimage.binary_dilation(img, iterations=30)

        # pokud je sirka obrazku mensi jak 465 tak masku optickeho disku zvetsime mene, jinak ji zvetsime vice aby pokryla opticky disk a jeho okoli 
        if (masked_img.shape[1] < 465):
            img = morphology.binary_dilation(img, selem=disk(20))
        else:
            img = morphology.binary_dilation(img, selem=disk(30))

        # masku optickeho disku aplikujeme na obrazek na vstupu cimz vymaskujeme opticky disk a zbudou nam jen svetle fleky pro svetle leze
        for rownum in range(len(masked_img)):
            for colnum in range(len(masked_img[rownum])):
                if (img[rownum][colnum] == True):
                    masked_img[rownum][colnum] = 0
            
        return masked_img

    def detect_disease(img):

        # zjistime pocet homogennich objektu
        img,num = measure.label(img, return_num=True, connectivity=None)

        # pri vetsim nozstvi svetlych lezi se asi bude jednat o dry age related macular degeneration
        if (num > 35):
            return "DRY AGE RELATED MACULAR DEGENERATION"

        # pokud je labelu vice jak > 0 a mene jak 31, tak se asi jedna o diabetickou retinopatii (ktera ma obvykle mene svetlych lezi)
        elif (num != 0):
            return "DIABETIC RETINOPATHY"

        # pri 0 labelech v obrazku nejsou priznaky nemoci
        else:
            return "NONE"

    def main(argv):
        '''
        Hlavni funkce programu ve ktere se zpracovavaji parametry, nacistaji se obrazky,
        zpracuji se a rozhodne se jestli v obrazku je nemoc nebo ne a pripadne jaka a nasledne obrazky s nemoci
        se ulozi do vystupniho adresare, kde v obrazcich budou vyznaceny priznaky nemoci
        @argv vstupni argumenty programu
        @return void
        '''

        input_dir = ''  # vstupni adresar s obrazky
        output_dir = '' # vystupni adresar kam se budou ukladat obrazky s nemoci
        place_to_output_only_image_with_disease = True

        # zpracujeme argumenty 
        try:
            opts, args = getopt.getopt(argv,"ahi:o:")

        except getopt.GetoptError:
            print ('usage: python3 program.py [-a] -i <inputDir> -o <outputDir>')
            print ('if param -a is missing, then in outputDir will be placed only images with some disease')
            sys.exit(2)

        for opt, arg in opts:
            if opt == '-h':
                print ('usage: python3 program.py [-a] -i <inputDir> -o <outputDir>')
                print ('if param -a is missing, then in outputDir will be placed only images with some disease')
                sys.exit()

            elif opt == "-i":
                input_dir = arg
                # doplnime lomitko na konec pokud tam neni
                if (input_dir.endswith('/') == False):
                    input_dir += "/"

            elif opt == "-o":
                output_dir = arg
                # doplnime lomitko na konec pokud tam neni
                if (output_dir.endswith('/') == False):
                    input_dir += "/"
            elif opt == "-a":
                place_to_output_only_image_with_disease = False
        
        list_of_images = [] # seznam jmen obrazku bez cesty

        # nacteme seznam jmen ze vstupniho adresare
        try:
            for file in os.scandir(input_dir):
                if file.is_file():
                    list_of_images.append(file.name)
        except Exception as e:
            print(e)
            sys.exit(2)

        list_of_images.sort() # abecedne seradime 

        i = 0
        # projdeme seznam obrazku a kazdy obrazek zpracujeme
        for image_filename in list_of_images:

            try:
                # nacteme obrazek ze vstupniho adresare, pokud nastane chyba oznamime to a cteme dalsi obrazek
                img = misc.imread(input_dir +  image_filename, False, 'RGB')
            except Exception as e:
                print(e)
                i = i + 1
                continue

            # normalizujeme velikost obrázku, pokud je to potřeba
            height, width, channels = img.shape

            if (width > 565):
                ratio = 565 / width
                img = misc.imresize(img, ratio)

            original_img = np.copy(img) # ulozime si puvodni obrazek (tedy puvodni zmenseny)

            # naejake upravy
            img = exposure.adjust_gamma(img)      # Gamma Korekce obrazu (zesvětlění nebo ztmavení obrazu)
            img = exposure.adjust_log(img)        # Logarithmická korekce obrazu
            img = exposure.rescale_intensity(img) # lepší kontrast (díky přeškálování (roztíhnutí nebo smrštění) úrovní intenzity)

            img_mask = get_mask(img) # ziskame masku, diyk ktere mame odfiltrovano pozadi sitnice (tedy cernou prazdnotu)
            
            # ZELENE ZVYRAZNENI MASKY, NASLEDNE JE TREBA ULOZIT OBRAZEK
            #img = mark_objects_margins_in_image(img_mask, original_img)         
            
            # extrahujeme zeleny RGB kanal, kde jsou videt nejlepe leze zlute a take opicky disk, s timto kanalem budeme dale pracovat
            green_channel = np.zeros((img.shape[0], img.shape[1]), dtype=np.uint8) # init 2D numpy array
            for rownum in range(len(img)):
                for colnum in range(len(img[rownum])):
                    green_channel[rownum][colnum] = img[rownum][colnum][1]

            # najdeme svetle leze (pozor detekuje se i opticky disk obvykle)
            img = find_yellow_lesions(np.copy(green_channel), img_mask)
            
            # odstranime opticky disk z najitych svetlych lezi
            img = mask_optic_disk(img, np.copy(green_channel))

            # ZELENE ZVYRAZNENI MASKY, NASLEDNE JE TREBA ULOZIT OBRAZEK
            #img = mark_objects_margins_in_image(img, original_img)

            # detekujeme nemoc na zaklade nalezenych svetlych lezi (coz jsou bile ostruvky v jinak cernem obrazku)
            disease = detect_disease(img)

            # trochu bile ostrivky zvetsime, aby se podle nich mohly lepe vyznacit leze v puvodnim obrazku
            img = ndimage.binary_dilation(img, iterations=3)

            if (disease == "DIABETIC RETINOPATHY"):
                prRed("DIABETIC RETINOPATHY has been detected in image: " + image_filename)          

            elif (disease == "DRY AGE RELATED MACULAR DEGENERATION"):
                prYellow("DRY AGE RELATED MACULAR DEGENERATION has been detected in image: " + image_filename)

            # pokud neni detakovana zadna nemoc a take si neprejem ukladat do vystupniho adresare obrazyk bez nemoci, poikracujeme dalsim obrazkem
            elif (disease == "NONE"):
                prCyan("NO disease has been detected in image: " + image_filename)
                if (place_to_output_only_image_with_disease == True):
                       print("")
                       continue

            # ZELENE ZVYRAZNENI SVETLCH LEZI
            img = mark_objects_margins_in_image(img, original_img)

            # ulozeni obrazku do vystupniho souboru
            try:
                misc.imsave(output_dir + image_filename + ".png",img)
                print('Image: ' + image_filename + ' saved to output directory.\n')
                sys.stdout.flush()
            except Exception as e:
                print(e)
                

            i = i + 1

    if __name__ == "__main__":

        '''sys.argv.append("-a")'''

        '''# toto je sada pro obrazky s diabetickou retinopatií
        sys.argv.append("-i")
        sys.argv.append("/home/jan/Plocha/step/")
        sys.argv.append("-o")
        sys.argv.append("/home/jan/Plocha/step-O/")'''
                       
        '''# toto je sada pro obrazky s diabetickou retinopatií
        sys.argv.append("-i")
        sys.argv.append("/home/jan/Plocha/BIO-PROJEKT/ddb1_v02_01/images/")
        sys.argv.append("-o")
        sys.argv.append("/home/jan/Plocha/BIO-vystup-obrazky3/")'''
        
        
        '''# macular degeneration
        sys.argv.append("-i")
        sys.argv.append("/home/jan/Plocha/BIO-PROJEKT/macular-degeneration/")
        sys.argv.append("-o")
        sys.argv.append("/home/jan/Plocha/BIO-vystup-obrazky-jeden/")'''

        '''# mix
        sys.argv.append("-i")
        sys.argv.append("/home/jan/Plocha/BIO-PROJEKT/mix/")
        sys.argv.append("-o")
        sys.argv.append("/home/jan/Plocha/BIO-vystup-obrazky-mix/")'''
        
        '''# toto je sada pro obrazky zdravé
        sys.argv.append("-i")
        sys.argv.append("/home/jan/Plocha/BIO-PROJEKT/DRIVE-DB/test/images/")
        sys.argv.append("-o")
        sys.argv.append("/home/jan/Plocha/BIO-vystup-obrazky-zdrave3/")'''
        
        main(sys.argv[1:])
